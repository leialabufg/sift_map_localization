#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/xfeatures2d/nonfree.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <fstream>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Odometry.h>

#define NO_DATA 127

void readme();


double diffclock(clock_t clock1,clock_t clock2){
    double diffticks=clock1-clock2;
    double diffms=(diffticks)/(CLOCKS_PER_SEC/1000);
    return diffms;
}

cv::Mat makeHoughLines(const cv::Mat img){

  cv::Mat dst(img.size(), img.type(), cv::Scalar::all(0));
  cv::Mat not_img;
  cv::bitwise_not ( img, not_img );
  std::vector<cv::Vec4i> lines;
  cv::HoughLinesP(not_img, lines, 1, CV_PI/180, 50, 50, 10 );

  for( size_t i = 0; i < lines.size(); i++ ){
    cv::Vec4i l = lines[i];
    cv::line( dst, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255), 3, CV_AA);
  }


  return dst;

}


void countAgrDis(cv::Mat& img_1, cv::Mat& img_2, int* out_agr, int* out_dis, cv::Mat* agreement_image = NULL){

  cv::Mat aggr_mat(img_1.size(), CV_8UC3, cv::Scalar::all(0));

  int& agree = *out_agr;
  int& disagree = *out_dis;

  agree = 0;
  disagree = 0;

  if(img_1.size() != img_2.size()){
    printf("ERROR: diff sizes\n");
    return;
  }
  for (int r = 0 ; r < img_1.size().height ; r++ ){
    for (int c = 0 ; c < img_1.size().width ; c++ ) {
      unsigned char img_1_val = img_1.at<unsigned char>(r, c);
      unsigned char img_2_val = img_2.at<unsigned char>(r, c);
      if(img_1_val != NO_DATA && img_2_val != NO_DATA){
        if(img_1_val == img_2_val){
          if(img_1_val > NO_DATA){
            aggr_mat.at<cv::Vec3b>(r, c)[1] = 255;
          }else{
            aggr_mat.at<cv::Vec3b>(r, c)[0] = 255;
          }
          ++agree;
        }else{
          aggr_mat.at<cv::Vec3b>(r, c)[2] = 255;
          ++disagree;
        }
      }
    }
  }

  if(agreement_image != NULL){
    *agreement_image = aggr_mat;
  }
  
}

int countRows(std::string fn){

   int number_of_lines = 0;
   std::string line;
   std::ifstream myfile(fn);

   while (std::getline(myfile, line))
      ++number_of_lines;

   return number_of_lines;
}

template <typename T>
double calcMedian(std::vector<T> scores)
{
  double median;
  size_t size = scores.size();

  sort(scores.begin(), scores.end());

  if (size  % 2 == 0)
  {
      median = (scores[size / 2 - 1] + scores[size / 2]) / 2;
  }
  else 
  {
      median = scores[size / 2];
  }

  return median;
}

template <typename T>
double getSampleStandardDeviation(std::vector<T> numbers){

  double sum = 0;
  for (auto it = numbers.begin(); it != numbers.end(); ++it)
  {
    sum += (double)(*it);
  }
  double avg = sum/numbers.size();
  sum = 0;
  for (auto it = numbers.begin(); it != numbers.end(); ++it)
  {
    sum += ((double)(*it) - avg)*((double)(*it) - avg);
  }
  avg = sum/(numbers.size() - 1);
  return sqrt(avg);
}

template <typename T>
double getMedianAbsoluteDeviation(std::vector<T> numbers){
  double median = calcMedian(numbers);
  std::vector<double> values;
  for(auto it = numbers.begin(); it != numbers.end(); ++it){
    values.push_back(std::abs((*it) - median));
  }
  return calcMedian(values);
}

template <typename T>
double getStandardDeviation(std::vector<T> numbers, T expectedResult){
  double sum = 0;
  sum = 0;
  for (auto it = numbers.begin(); it != numbers.end(); ++it)
  {
    sum += ((double)(*it) - expectedResult)*((double)(*it) - expectedResult);
  }
  double s = sum/numbers.size();
  return sqrt(s);
}


template <typename T>
double getAvg(std::vector<T> numbers){
  double sum = 0;
  for (auto it = numbers.begin(); it != numbers.end(); ++it)
  {
    sum += (double)(*it);
  }
  return sum/numbers.size();
}



int countColumns(std::string fn){


  int number_of_columns = 0;
  std::string line;
  std::ifstream myfile(fn);
  std::getline(myfile, line);
  for(int i = 0; i < line.size(); ++i){
    if(line[i] == ' '){
      ++number_of_columns;
    }
  }

  return number_of_columns;

}

cv::Point2f operator*(cv::Mat M, const cv::Point2f& p)
{ 
    cv::Mat_<double> src(3/*rows*/,1 /* cols */); 

    src(0,0)=p.x; 
    src(1,0)=p.y; 
    src(2,0)=1.0; 

    cv::Mat_<double> dst = M*src; //USE MATRIX ALGEBRA 
    return cv::Point2f(dst(0,0),dst(1,0)); 
} 


double checkQuality(std::vector<cv::Point2f> pt1_vec, std::vector<cv::Point2f> pt2_vec, cv::Mat rotationMatrix, double translationX, double translationY){

  if(pt2_vec.size() != pt1_vec.size()){
    printf("ERROR: diff sizes\n");
    return -1;
  }

  std::vector<cv::Point2f> pt1_vec_translated;


   for(auto it = pt1_vec.begin(); it != pt1_vec.end(); it++){
    cv::Point2f p = (*it);
    p.x += translationX;
    p.y += translationY;
    pt1_vec_translated.push_back(rotationMatrix*(p));
   }

  std::vector<double> distances;

  for(int i = 0; i < pt2_vec.size(); i ++){
    double dx;
    double dy;
    double angle;
    int sign;
    cv::Point2f p2mp1;
    cv::Point2f ihat(1,0);
    p2mp1 = pt2_vec[i] - pt1_vec_translated[i];
    double p2mp1size = norm(p2mp1);

    p2mp1.x = p2mp1.x/p2mp1size;
    p2mp1.y = p2mp1.y/p2mp1size;

    distances.push_back(p2mp1size);
  }

  double median = calcMedian(distances);

  return median;

}


cv::Mat& clampMap(cv::Mat& I){
    cv::Mat lookUpTable(1, 256, CV_8U);
    uchar* p = lookUpTable.data;
    for( int i = 0; i < 256; ++i)
        if(i <= 95){
          p[i] = 0;
        }else if(i > 220){
          p[i] = 255;
        }else{
          p[i] = 127;
        }
    LUT(I, lookUpTable, I);
    return I;
}


cv::Mat loadMap(unsigned int rows, unsigned int columns, const char* fname){


  cv::Mat map(rows,columns, CV_8UC1);
  std::ifstream ifs(fname);

  if ( ! ifs ) 
    return map;
  
  unsigned int i,j;
  unsigned int tmp;
  for ( i = 0 ; i < rows ; i++ )
    for ( j = 0 ; j < columns ; j++ ) {
      ifs >> tmp;
      if(tmp == 0){
        tmp = 1;
      }
      map.at<unsigned char>(i, j) = static_cast<unsigned char>(tmp);
    }
  ifs.close();
  return map;

}

cv::Mat overlap_maps(cv::Mat &m1, cv::Mat& m2) {

  cv::Mat outmat = m1.clone();

  int r = outmat.rows;
  int c = outmat.cols;
  unsigned int i, j;
  for( i = 0 ; i < r ; i++ )
      for( j = 0 ; j < c ; j++ ){
          if(m2.at<unsigned char>(i,j) < 90)
            outmat.at<unsigned char>(i,j) = 0;
          else if( ( m2.at<unsigned char>(i,j) >= 220 ) && ( outmat.at<unsigned char>(i,j) >= 90 || outmat.at<unsigned char>(i,j) < 220 ) )
            outmat.at<unsigned char>(i,j) = 255;
      }

  return outmat;
}

int mapMerging(const cv::Mat& img_1_input,const cv::Mat& img_2_input, cv::Mat& out_final_transformation, int number_of_passes = 1)
{

  cv::Mat img_1 = img_1_input.clone();
  cv::Mat img_2 = img_2_input.clone();

  out_final_transformation = cv::Mat::eye(3, 3, CV_64F);

  int minHessian = 50;

  cv::Ptr<cv::Feature2D> f2d = cv::xfeatures2d::SIFT::create(minHessian);

  cv::Mat img1_blurred;
  cv::Mat img2_blurred;

  std::vector<cv::KeyPoint> keypoints_point_img_1, keypoints_point_img_2;

  std::vector< cv::DMatch > good_matches;

  for(int pass_it = 0; pass_it < number_of_passes; pass_it++){


    cv::blur( img_1, img1_blurred, cv::Size( 7, 7 ), cv::Point(-1,-1));
    cv::blur( img_2, img2_blurred, cv::Size( 7, 7 ), cv::Point(-1,-1));

    //-- Step 1: Detect the keypoints using SIFT Detector


    keypoints_point_img_1.clear();
    keypoints_point_img_2.clear();

    f2d->detect( img1_blurred, keypoints_point_img_1 );
    f2d->detect( img2_blurred, keypoints_point_img_2 );

    //-- Step 2: Calculate descriptors (feature vectors)

    cv::Mat descriptors_point_img_1, descriptors_point_img_2;

    f2d->compute( img1_blurred, keypoints_point_img_1, descriptors_point_img_1 );
    f2d->compute( img2_blurred, keypoints_point_img_2, descriptors_point_img_2 );

    //-- Step 3: Matching descriptor vectors using FLANN matcher

    if ( descriptors_point_img_1.empty() || descriptors_point_img_2.empty() ){
      printf("Not enough good matches\n");
      return 0;
    }

    cv::FlannBasedMatcher matcher;
    std::vector< cv::DMatch > matches;
    matcher.match( descriptors_point_img_1, descriptors_point_img_2, matches );

    double max_dist = 0; double min_dist = 100; int aux_min_dist_id = 0; int min_dist_id = 0;

    //-- Quick calculation of max and min distances between keypoints 

    for( int i = 0; i < descriptors_point_img_1.rows; i++ )
    { double dist = matches[i].distance;
      if( dist < min_dist ){
        min_dist = dist;
        aux_min_dist_id = i;
      }
      if( dist > max_dist ) max_dist = dist;
    }

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )

    good_matches.clear();


    for( int i = 0; i < descriptors_point_img_1.rows; i++ ){
     if( matches[i].distance <= (3*min_dist)) {
        if(i == aux_min_dist_id){
          min_dist_id = good_matches.size();
        }
        good_matches.push_back( matches[i]); 
      }
    }
    if(good_matches.size() <= 1){
      printf("Not enough good matches\n");
      return 0;
    }

    //-- Localize the point_img_1ect

    std::vector<cv::Point2f> point_img_1;
    std::vector<cv::Point2f> point_img_2;

    // printf("good_matches.size = %lu\n", good_matches.size() );
    for( int i = 0; i < good_matches.size(); i++ )
    {
      //-- Get the keypoints from the good matches
      point_img_1.push_back( keypoints_point_img_1[ good_matches[i].queryIdx ].pt );
      point_img_2.push_back( keypoints_point_img_2[ good_matches[i].trainIdx ].pt );

    }

    double translationX = point_img_2[min_dist_id].x - point_img_1[min_dist_id].x;
    double translationY = point_img_2[min_dist_id].y - point_img_1[min_dist_id].y;

    cv::Point2f pv(point_img_2[min_dist_id].x, point_img_2[min_dist_id].y);

    double pointQuality = std::numeric_limits<double>::max(); //infinite
    
    cv::Mat rot_mat;
    double selectedAngle;
    double selectedRatio;
    double selectedTranslationX;
    double selectedTranslationY;
  

    for(int i = 0; i < good_matches.size(); i++){
      if(i == min_dist_id){ // avoid second point to the the same as the first point
        continue;
      }else{
        if(point_img_1[i].x != point_img_1[min_dist_id].x && point_img_1[i].y != point_img_1[min_dist_id].y// This if avoids the second point to be a duplicate of the first point
        && (point_img_1[min_dist_id].x + translationX != point_img_2[i].x) && (point_img_1[min_dist_id].y + translationY != point_img_2[i].y)){

          cv::Point2f p1(point_img_1[i].x + translationX, point_img_1[i].y + translationY);
          cv::Point2f p2(point_img_2[i].x, point_img_2[i].y);

          cv::Point2f p1mpv = p1 - pv;
          cv::Point2f p2mpv = p2 - pv;

          double sizep1mpv = norm(p1mpv);
          p1mpv.x = p1mpv.x/sizep1mpv;
          p1mpv.y = p1mpv.y/sizep1mpv;
          
          double sizep2mpv = norm(p2mpv);
          p2mpv.x = p2mpv.x/sizep2mpv;
          p2mpv.y = p2mpv.y/sizep2mpv;

          double ratio = 1.0;//sizep2mpv/sizep1mpv;
          double dotProduct = p1mpv.ddot(p2mpv);
          double cross = p1mpv.cross(p2mpv);
          int sign;
          if(cross >= 0){
            sign = 1;
          }else{
            sign = -1;
          }

          double angle = acos(dotProduct);
          angle = -sign*(180*angle)/M_PI; //Radians to Degrees, y grows top down, so z is to the screen, that is why the sign is negated.

          rot_mat = cv::getRotationMatrix2D( pv, angle, ratio);
          double quality = checkQuality(point_img_1, point_img_2, rot_mat, translationX, translationY);
          if(quality < pointQuality){ //if these points are the best until now
            pointQuality = quality;
            selectedAngle = angle;
            selectedRatio = ratio;
            selectedTranslationX = translationX;
            selectedTranslationY = translationY;
          }
        }
      }
    }

    if(pointQuality == std::numeric_limits<double>::max()){
      printf("Not enough good matches\n");
      printf("ratio = %f\n", selectedRatio );
      return 0;
    }


    cv::Mat translation_matrix = (cv::Mat_<double>(3,3) << 1, 0, selectedTranslationX, 0, 1, selectedTranslationY, 0, 0, 1); //translates secondary image to allign the pivots
    cv::Mat rotation_and_scale_matrix = cv::getRotationMatrix2D( pv, selectedAngle, selectedRatio); //Rotate around the pivot and scales.

    //Transforming from 2x3 to 3x3 Matrix
    cv::Mat row = (cv::Mat_<double>(1,3) << 0, 0, 1);
    rotation_and_scale_matrix.push_back(row);
    // -------------------------------------

    cv::Mat transformation = rotation_and_scale_matrix * translation_matrix;

    if(pass_it != number_of_passes - 1){
      cv::warpPerspective(img_1, img_1, transformation, img_2.size(), CV_INTER_AREA, cv::BORDER_CONSTANT, cv::Scalar::all(NO_DATA));
    }
    out_final_transformation = transformation * out_final_transformation;
  }


  cv::Mat img_matches;
  cv::drawMatches(img1_blurred, keypoints_point_img_1, img2_blurred, keypoints_point_img_2, good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),  std::vector< char >(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
  cv::imshow("matches", img_matches);


  return 1;
}


cv::Mat map_1;
cv::Mat map_2;


void callback_map_1(const nav_msgs::OccupancyGridConstPtr& map){
    int size_x = map->info.width;
    int size_y = map->info.height;

    const std::vector<int8_t>& map_data (map->data);
    

    cv::Mat temp_mat(size_y, size_x, CV_8UC1, cv::Scalar(0));

    unsigned char* map_mat_data_p = (unsigned char*) temp_mat.data;

    //We have to flip around the y axis, y for image starts at the top and y for map at the bottom
    int size_y_rev = size_y-1;

    ///std::cout<< "size_y = " << size_y << ", size_x = " << size_x << std::endl;

    for (int y = size_y_rev; y >= 0; --y){

        int idx_map_y = size_x * (size_y - y - 1);
        int idx_img_y = size_x * y;

        for (int x = 0; x < size_x; ++x){

            int idx = idx_img_y + x;

            switch (map_data[idx_map_y + x]){
            case -1:
                map_mat_data_p[idx] = 127;
            break;

            case 0:
                map_mat_data_p[idx] = 255;
            break;

            case 100:
                map_mat_data_p[idx] = 0;
            break;
            default:
                map_mat_data_p[idx] = 127;
            }
        }
    }


    //cv::resize(temp_mat, temp_mat, cv::Size(530, 530));
    //temp_mat = makeHoughLines(temp_mat);
    temp_mat.copyTo(map_1);

}




void callback_map_2(const nav_msgs::OccupancyGridConstPtr& map){
    int size_x = map->info.width;
    int size_y = map->info.height;

    const std::vector<int8_t>& map_data (map->data);

    cv::Mat temp_mat(size_y, size_x, CV_8U);

    unsigned char* map_mat_data_p = (unsigned char*) temp_mat.data;

    //We have to flip around the y axis, y for image starts at the top and y for map at the bottom
    int size_y_rev = size_y-1;

    for (int y = size_y_rev; y >= 0; y--){

        int idx_map_y = size_x * (size_y -y - 1);
        int idx_img_y = size_x * y;

        for (int x = 0; x < size_x; ++x){

            int idx = idx_img_y + x;

            switch (map_data[idx_map_y + x]){
            case -1:
                map_mat_data_p[idx] = 127;
            break;

            case 0:
                map_mat_data_p[idx] = 255;
            break;
            case 100:
                map_mat_data_p[idx] = 0;
            break;
            default:
                map_mat_data_p[idx] = 127;
            }
        }
    }

    //temp_mat = makeHoughLines(temp_mat);
    //cv::resize(temp_mat, temp_mat, cv::Size(530, 530));
    temp_mat.copyTo(map_2);    

}




int main(int argc, char *argv[]){

    ros::init(argc, argv, "sift_map_localization");

    ros::NodeHandle np("~"), n;

    //np.getParam("local_map", local_map_name); //focal length in millimeters
    //np.getParam("global_map", global_map_name); // pixel size in millimiters
    

    ros::Subscriber subscriber = n.subscribe<nav_msgs::OccupancyGrid>("/robot_0/map", 1, callback_map_1);
    ros::Subscriber subscriber_2 = n.subscribe<nav_msgs::OccupancyGrid>("/robot_1/map", 1, callback_map_2);
    ros::Publisher publisher = np.advertise<sensor_msgs::Image>("maps_merged", 1);


    ros::Rate loop_rate(1);

    while (ros::ok()){
        ros::spinOnce();
        //if(publisher.getNumSubscribers() > 0){
            if (!map_1.empty() && !map_2.empty()){
                //std::cout<< local_map << std::endl;

               // cv::imshow("local_map", local_map);
                //cv::imshow("global_map", global_map);

                cv::Mat final_transformation;
                int number_of_passes = 2;


                int okMerging = mapMerging( map_1, map_2, final_transformation, number_of_passes);

                if(okMerging){

                    cv::Mat map_1_transformed;
                    cv::warpPerspective(map_1, map_1_transformed, final_transformation, map_2.size(), CV_INTER_AREA, cv::BORDER_CONSTANT, cv::Scalar::all(NO_DATA));
                    int agree;
                    int disagree;

                    countAgrDis(map_1_transformed, map_2, &agree, &disagree);

                    double agreementRatio = agree > 0?( (double)agree)/(agree + disagree) : 0.0;


                    if(agreementRatio > 0.85){
                      cv::Mat overlaped_maps = overlap_maps(map_1_transformed, map_2);
                      sensor_msgs::ImagePtr msg1 = cv_bridge::CvImage(std_msgs::Header(), "mono8", overlaped_maps).toImageMsg();
                      publisher.publish(msg1);
                    }
                    ROS_INFO("Agreement Ratio = %f\n", agreementRatio);


                      



                }


            }
         //}
         loop_rate.sleep();
    }

    return 0;
}